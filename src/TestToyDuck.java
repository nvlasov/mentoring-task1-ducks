import entity.DirectionEnum;
import entity.Duck;
import entity.RealDuck;
import entity.ToyDuck;

public class TestToyDuck {

    public static void main(String[] args) {

        // Toy duck can't fly.
        // After 10 steps it will did 5 quacks and then switch off without recharging and
        // without continue doing all other 15 steps from the loop
        Duck toyDuck = new ToyDuck();
        toyDuck.performFly();
        for (int i = 0; i < 25; i++) {
            toyDuck.move(DirectionEnum.DOWN);
        }


    }
}
