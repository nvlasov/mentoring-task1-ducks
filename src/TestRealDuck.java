import entity.DirectionEnum;
import entity.Duck;
import entity.RealDuck;

public class TestRealDuck {

    public static void main(String[] args) {

        // Real duck will drink water while swimming and eat food while walking or flying.
        // After eating or drinking duck will continue moving.
        // It can fly.
        Duck realDuck = new RealDuck();
        realDuck.startSwimming();
        for (int i = 0; i < 11; i++) {
            realDuck.move(DirectionEnum.DOWN);
        }
        realDuck.stopSwimming();
        for (int i = 0; i < 11; i++) {
            realDuck.move(DirectionEnum.LEFT);
        }
        realDuck.performFly();
    }

}
