package behaviour;

/**
 * For not flying things
 */
public class FlyNoWay implements FlyBehaviour {
    @Override
    public void fly() {
        System.out.println("I can't fly, only waving");
    }
}
