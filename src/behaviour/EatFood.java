package behaviour;

import entity.Duck;

/**
 * Hungry behaviour for {@link entity.RealDuck}
 */
public class EatFood implements HungryBehaviour {

    @Override
    public void hungryNotifiction() {
        System.out.println("Need food or water!");
    }

    @Override
    public void eat(Duck duck) {
        System.out.println(duck.isSwimming ? "I'm drinking water because I'm swimming!" : "I'm eating food!");
        duck.isHungry = false;
    }

}
