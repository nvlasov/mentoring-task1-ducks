package behaviour;

import entity.Duck;

/**
 * Interface for hungry behaviour.
 */
public interface HungryBehaviour {

    public void hungryNotifiction();

    public void eat(Duck duck);

}
