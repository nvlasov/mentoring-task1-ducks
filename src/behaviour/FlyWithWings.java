package behaviour;

/**
 * For entities which can fly using wings
 */
public class FlyWithWings implements FlyBehaviour {
    @Override
    public void fly() {
        System.out.println("I can fly!");
    }
}
