package behaviour;

/**
 * Fly behaviour interface
 */
public interface FlyBehaviour {

    public void fly();

}
