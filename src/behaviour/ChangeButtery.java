package behaviour;

import entity.Duck;

/**
 * Hungry behaviour for {@link entity.ToyDuck}
 */
public class ChangeButtery implements HungryBehaviour {

    @Override
    public void hungryNotifiction() {
        System.out.println("Need new buttery!");
    }

    @Override
    public void eat(Duck duck) {
        for (int i = 0; i < 5; i++) {
            duck.quack();
        }
        System.out.println("I can't eat, I'm switching off ...");
    }


}
