package entity;

import behaviour.FlyBehaviour;
import behaviour.HungryBehaviour;

/**
 * Abstract class for Ducks.
 */
public abstract class Duck {

//    Interface for behaviours which are depends on type of duck. That's why moved to interface and decided to use Strategy Pattern
    HungryBehaviour hungryBehaviour;
    FlyBehaviour flyBehaviour;

    public boolean isSwimming = false;
    int countSteps = 0;
    public boolean isHungry = false;

    public abstract void display();

//    common possibility for all ducks
    public void startSwimming() {
        isSwimming = true;
        System.out.println("Start swimming ...");
    }

//    common possibility for all ducks
    public void stopSwimming() {
        isSwimming = false;
        System.out.println("Land! Stop swimming. Start moving ...");
    }

//    All ducks can move until they are not hungry.
//    They become hungry after 10 steps.
    public void move(DirectionEnum direction) {
        if (countSteps == 10) {
            isHungry = true;
            performEating();
            countSteps = 0;
        }
        if (!isHungry) {
            if (direction.equals(DirectionEnum.DOWN)) {
                System.out.println("1 step down");
            } else if (direction.equals(DirectionEnum.UP)) {
                System.out.println("1 step up");
            } else if (direction.equals(DirectionEnum.LEFT)) {
                System.out.println("1 step left");
            } else if (direction.equals(DirectionEnum.RIGHT)) {
                System.out.println("1 step right");
            }
            countSteps++;
        }
    }

    public void performFly() {
        flyBehaviour.fly();
    }

    public void performEating() {
        System.out.println("I did 10 steps");
        hungryBehaviour.hungryNotifiction();
        hungryBehaviour.eat(this);
    }

    public void quack() {
        System.out.println("Quack!");
    }

}
