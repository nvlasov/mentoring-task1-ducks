package entity;

import behaviour.ChangeButtery;
import behaviour.FlyNoWay;

/**
 * It is a toy duck.
 */
public class ToyDuck extends Duck {

    public ToyDuck() {
        flyBehaviour = new FlyNoWay();
        hungryBehaviour = new ChangeButtery();
        display();
    }

    @Override
    public void display() {
        System.out.println("I'm a TOY duck!");
    }
}
