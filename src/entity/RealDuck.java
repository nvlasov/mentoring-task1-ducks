package entity;

import behaviour.EatFood;
import behaviour.FlyWithWings;

/**
 * It is a real duck.
 */
public class RealDuck extends Duck {

    public RealDuck() {
        flyBehaviour = new FlyWithWings();
        hungryBehaviour = new EatFood();
        display();
    }
    
    @Override
    public void display() {
        System.out.println("I'm a REAL duck!");
    }
}
