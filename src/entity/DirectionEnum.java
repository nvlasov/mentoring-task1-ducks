package entity;

/**
 * Just Enum for moving 4 directions.
 */
public enum DirectionEnum {

    UP, DOWN, LEFT, RIGHT

}
